# Nex Ansible Role

This role installs and configures the [Nex](https://gitlab.com/mergetb/tech/nex) 
DHCP/DNS system.

## Variables

| name | required | default | description |
| ---- | ---------| ------- | ----------- |
| interface | **yes** | | the interface for Nex dhcp/dns to listen on |
| domain | **yes** | | the domain name to use |
| nexd.addr | no | 0.0.0.0 | the address for nexd to listen on |
| nexd.port | no | 6000 | the port for nexd to listen on |
| etcd.host | no | db | the etcd database host address to connect to |
| etcd.port | no | 2379 | the etcd database host port to connect to |
| etcd.cacert | no | /etc/nex/ca.pem | ca cert used to connect to a secured etcd instance|
| etcd.cert | no | /etc/nex/db.pem | tls cert used to connect to a secured etcd instance|
| etcd.key | no | /etc/nex/db-key.pem | tls key used to connect to a secured etcd instance|

## Notes

If you are using an unsecured etcd database, you will need to explicitly provide
empty string values for `etcd.cacert`, `etcd.cert` and `etcd.key`.

Changing any top level configuration object, such as `etcd` means you must
specify all parameters for that object. For example setting just `etcd.host`
will leave `etcd.port` empty and the configuration will fail. Sorry

## Examples

A minimal nex configuration.

```yaml
- import_role:
    name: nex
  vars:
    domain: mini.net
    interface: eth1
```
